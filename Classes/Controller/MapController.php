<?php
namespace HIVE\HiveCptCntGoogleMap\Controller;

/***
 *
 * This file is part of the "hive_cpt_cnt_google_map" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * MapController
 */
class MapController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * mapRepository
     *
     * @var \HIVE\HiveCptCntGoogleMap\Domain\Repository\MapRepository
     * @inject
     */
    protected $mapRepository = null;

    /**
     * action render
     *
     * @return void
     */
    public function renderAction()
    {
        $iMapUid = $this->settings['oHiveCptCntGoogleMap'];
        $iPluginUid = $this->configurationManager->getContentObject()->data['uid'];
        $this->view->assign('iMapUid', $iMapUid);
        $this->view->assign('iPluginUid', $iPluginUid);
    }
}
