<?php
namespace HIVE\HiveCptCntGoogleMap\Domain\Repository;

/***
 *
 * This file is part of the "hive_cpt_cnt_google_map" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Maps
 */
class MapRepository extends \HIVE\HiveCfgRepository\Domain\Repository\AbstractRepository
{
    public function initializeObject()
    {
        $sUserFuncModel = 'HIVE\\HiveCptCntGoogleMap\\Domain\\Model\\Map';
        $sUserFuncPlugin = 'tx_hivecptcntgooglemap';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }
}
