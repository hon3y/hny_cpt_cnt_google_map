<?php
namespace HIVE\HiveCptCntGoogleMap\Domain\Model;

/***
 *
 * This file is part of the "hive_cpt_cnt_google_map" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Map
 */
class Map extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * render
     *
     * @var string
     */
    protected $render = '';

    /**
     * coordinate
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtAddress\Domain\Model\Coordinate>
     */
    protected $coordinate = null;

    /**
     * centerCoordinate
     *
     * @var \HIVE\HiveExtAddress\Domain\Model\Coordinate
     */
    protected $centerCoordinate = null;

    /**
     * markerIcon
     *
     * @var \HIVE\HiveCptCntGoogleMap\Domain\Model\MarkerIcon
     */
    protected $markerIcon = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->coordinate = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Adds a Coordinate
     *
     * @param \HIVE\HiveExtAddress\Domain\Model\Coordinate $coordinate
     * @return void
     */
    public function addCoordinate(\HIVE\HiveExtAddress\Domain\Model\Coordinate $coordinate)
    {
        $this->coordinate->attach($coordinate);
    }

    /**
     * Removes a Coordinate
     *
     * @param \HIVE\HiveExtAddress\Domain\Model\Coordinate $coordinateToRemove The Coordinate to be removed
     * @return void
     */
    public function removeCoordinate(\HIVE\HiveExtAddress\Domain\Model\Coordinate $coordinateToRemove)
    {
        $this->coordinate->detach($coordinateToRemove);
    }

    /**
     * Returns the coordinate
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtAddress\Domain\Model\Coordinate> $coordinate
     */
    public function getCoordinate()
    {
        return $this->coordinate;
    }

    /**
     * Sets the coordinate
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtAddress\Domain\Model\Coordinate> $coordinate
     * @return void
     */
    public function setCoordinate(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $coordinate)
    {
        $this->coordinate = $coordinate;
    }

    /**
     * Returns the centerCoordinate
     *
     * @return \HIVE\HiveExtAddress\Domain\Model\Coordinate $centerCoordinate
     */
    public function getCenterCoordinate()
    {
        return $this->centerCoordinate;
    }

    /**
     * Sets the centerCoordinate
     *
     * @param \HIVE\HiveExtAddress\Domain\Model\Coordinate $centerCoordinate
     * @return void
     */
    public function setCenterCoordinate(\HIVE\HiveExtAddress\Domain\Model\Coordinate $centerCoordinate)
    {
        $this->centerCoordinate = $centerCoordinate;
    }

    /**
     * Returns the render
     *
     * @return string render
     */
    public function getRender()
    {
        return $this->render;
    }

    /**
     * Sets the render
     *
     * @param int $render
     * @return void
     */
    public function setRender($render)
    {
        $this->render = $render;
    }

    /**
     * Returns the markerIcon
     *
     * @return \HIVE\HiveCptCntGoogleMap\Domain\Model\MarkerIcon $markerIcon
     */
    public function getMarkerIcon()
    {
        return $this->markerIcon;
    }

    /**
     * Sets the markerIcon
     *
     * @param \HIVE\HiveCptCntGoogleMap\Domain\Model\MarkerIcon $markerIcon
     * @return void
     */
    public function setMarkerIcon(\HIVE\HiveCptCntGoogleMap\Domain\Model\MarkerIcon $markerIcon)
    {
        $this->markerIcon = $markerIcon;
    }
}
