<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveCptCntGoogleMap',
            'Hivecptcntgooglemaprender',
            'hive_cpt_cnt_google_map :: Render'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveCptCntGoogleMap',
            'Hivecptcntgooglemaprequest',
            'hive_cpt_cnt_google_map :: Request'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_cpt_cnt_google_map', 'Configuration/TypoScript', 'hive_cpt_cnt_google_map');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntgooglemap_domain_model_map', 'EXT:hive_cpt_cnt_google_map/Resources/Private/Language/locallang_csh_tx_hivecptcntgooglemap_domain_model_map.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntgooglemap_domain_model_map');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntgooglemap_domain_model_infowindow', 'EXT:hive_cpt_cnt_google_map/Resources/Private/Language/locallang_csh_tx_hivecptcntgooglemap_domain_model_infowindow.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntgooglemap_domain_model_infowindow');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntgooglemap_domain_model_markericon', 'EXT:hive_cpt_cnt_google_map/Resources/Private/Language/locallang_csh_tx_hivecptcntgooglemap_domain_model_markericon.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntgooglemap_domain_model_markericon');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntgooglemap_domain_model_request', 'EXT:hive_cpt_cnt_google_map/Resources/Private/Language/locallang_csh_tx_hivecptcntgooglemap_domain_model_request.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntgooglemap_domain_model_request');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function($extkey)
    {

        /***************
         * Add page TSConfig
         */
        $pageTsConfig = \TYPO3\CMS\Core\Utility\GeneralUtility::getUrl(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($extkey) . 'Configuration/TsConfig/Page/config.txt');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig($pageTsConfig);

    },
    $_EXTKEY
);

$extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY));
$pluginName = strtolower('Hivecptcntgooglemaprender');
$pluginSignature = $extensionName.'_'.$pluginName;
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'.$_EXTKEY . '/Configuration/FlexForms/Config.xml');