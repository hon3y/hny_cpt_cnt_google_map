<?php
namespace HIVE\HiveCptCntGoogleMap\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class InfoWindowTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveCptCntGoogleMap\Domain\Model\InfoWindow
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveCptCntGoogleMap\Domain\Model\InfoWindow();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImageReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getImage()
        );
    }

    /**
     * @test
     */
    public function setImageForFileReferenceSetsImage()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImage($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'image',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTextReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getText()
        );
    }

    /**
     * @test
     */
    public function setTextForStringSetsText()
    {
        $this->subject->setText('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'text',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCoordinateReturnsInitialValueForCoordinate()
    {
    }

    /**
     * @test
     */
    public function setCoordinateForCoordinateSetsCoordinate()
    {
    }
}
