<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntGoogleMap',
            'Hivecptcntgooglemaprender',
            [
                'Map' => 'render'
            ],
            // non-cacheable actions
            [
                'Map' => '',
                'Request' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntGoogleMap',
            'Hivecptcntgooglemaprequest',
            [
                'Request' => 'request'
            ],
            // non-cacheable actions
            [
                'Request' => 'request'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivecptcntgooglemaprender {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_cpt_cnt_google_map') . 'Resources/Public/Icons/user_plugin_hivecptcntgooglemaprender.svg
                        title = LLL:EXT:hive_cpt_cnt_google_map/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_google_map_domain_model_hivecptcntgooglemaprender
                        description = LLL:EXT:hive_cpt_cnt_google_map/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_google_map_domain_model_hivecptcntgooglemaprender.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntgooglemap_hivecptcntgooglemaprender
                        }
                    }
                    hivecptcntgooglemaprequest {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_cpt_cnt_google_map') . 'Resources/Public/Icons/user_plugin_hivecptcntgooglemaprequest.svg
                        title = LLL:EXT:hive_cpt_cnt_google_map/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_google_map_domain_model_hivecptcntgooglemaprequest
                        description = LLL:EXT:hive_cpt_cnt_google_map/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_google_map_domain_model_hivecptcntgooglemaprequest.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntgooglemap_hivecptcntgooglemaprequest
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function($extKey)
    {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hivecptcntgooglemaprender >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hivecptcntgooglemaprender {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Google Map
                            description = Google Map for Coordinates
                            tt_content_defValues {
                                CType = list
                                list_type = hivecptcntgooglemap_hivecptcntgooglemaprender
                            }
                        }
                        show := addToList(hivecptcntgooglemaprender)
                    }

                }
            }'
        );

    }, $_EXTKEY
);

/*
* Hooks
*/
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('realurl')) {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['extensionConfiguration']['hive_cpt_cnt_google_map'] =
        \HIVE\HiveCptCntGoogleMap\Hooks\RealUrlAutoConfiguration::class . '->addConfig';
}

/*
 * Exclude parameters from cHash validation
 */
$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_hivecptcntgooglemap_hivecptcntgooglemaprequest[map-api]';
$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_hivecptcntgooglemap_hivecptcntgooglemaprequest[marker_icon-api]';
$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_hivecptcntgooglemap_hivecptcntgooglemaprequest[info_window-api]';