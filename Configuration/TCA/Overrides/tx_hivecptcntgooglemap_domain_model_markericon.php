<?php
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');

/*
 * Icon
 */
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_16x16.svg';

/*
 * Coordinate
 */
$sColumn = 'coordinate';
$sTable = 'tx_hiveextaddress_domain_model_coordinate';
$sUserFuncModel = 'HIVE\\HiveExtAddress\\Domain\\Model\\Coordinate';
$sUserFuncPlugin = 'tx_hiveextaddress';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'group',
    'internal_type' => 'db',
    'allowed' => $sTable,
    'foreign_table' => $sTable,
    'foreign_table_where' => 'AND sys_language_uid IN (-1,0) AND deleted = 0 AND hidden = 0',
    'size' => 1,
    'minitems' => 0,
    'maxitems' => 1,
    'multiple' => 0,
    'suggestOptions' => [
        'default' => [
            'searchWholePhrase' => 1,
        ],
        $sTable => [
            'searchCondition' => 'sys_language_uid IN (-1,0)'
        ],
    ],
];
$sPidList = (string) HIVE\HiveUserfuncs\UserFunc\StorageUserFunc::getStoragePidListForModelInPlugin($sUserFuncModel, $sUserFuncPlugin);
if ($sPidList != '') {
    $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['suggestOptions']['default']['pidList'] = $sPidList;
}

/*
 * Marker icon
 */
$sColumn = 'marker_icon';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'select',
    'renderType' => 'selectSingle',
    'items' => [
        ['MAP_PIN','MAP_PIN'],
        ['SQUARE_PIN','SQUARE_PIN'],
        ['SHIELD','SHIELD'],
        ['ROUTE','ROUTE'],
        ['SQUARE','SQUARE'],
        ['SQUARE_ROUNDED','SQUARE_ROUNDED'],
    ],
    'default' => 'MAP_PIN',
];

/*
 * Marker size
 */
$sColumn = 'icon_size';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'select',
    'renderType' => 'selectSingle',
    'items' => [
        ['32x32','32'],
        ['64x64','64'],
        ['128x128','128'],
    ],
    'default' => 'MAP_PIN',
];

/*
 * Marker anchor
 */
$sColumn = 'icon_anchor';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'select',
    'renderType' => 'selectSingle',
    'items' => [
        ['bottom left','bottom left'],
        ['bottom center','bottom center'],
        ['bottom right','bottom right'],
        ['center right','center right'],
        ['top right','top right'],
        ['top center','top center'],
        ['top left','top left'],
        ['center left','center left'],
        ['center center','center center'],
    ],
    'default' => 'bottom center',
];