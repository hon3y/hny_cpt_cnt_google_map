<?php
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');

/*
 * Icon
 */
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_16x16.svg';

/*
 * Coordinate
 */
$sColumn = 'center_coordinate';
$sTable = 'tx_hiveextaddress_domain_model_coordinate';
$sUserFuncModel = 'HIVE\\HiveExtAddress\\Domain\\Model\\Coordinate';
$sUserFuncPlugin = 'tx_hiveextaddress';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'group',
    'internal_type' => 'db',
    'allowed' => $sTable,
    'foreign_table' => $sTable,
    'foreign_table_where' => 'AND sys_language_uid IN (-1,0) AND deleted = 0 AND hidden = 0',
    'size' => 1,
    'minitems' => 1,
    'maxitems' => 1,
    'multiple' => 0,
    'suggestOptions' => [
        'default' => [
            'searchWholePhrase' => 1,
        ],
        $sTable => [
            'searchCondition' => 'sys_language_uid IN (-1,0)'
        ],
    ],
];
$sPidList = (string) HIVE\HiveUserfuncs\UserFunc\StorageUserFunc::getStoragePidListForModelInPlugin($sUserFuncModel, $sUserFuncPlugin);
if ($sPidList != '') {
    $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['suggestOptions']['default']['pidList'] = $sPidList;
}

/*
 * MarkerIcon
 */
$sColumn = 'marker_icon';
$sTable = 'tx_hivecptcntgooglemap_domain_model_markericon';
$sUserFuncModel = 'HIVE\\HiveCptCntGoogleMap\\Domain\\Model\\MarkerIcon';
$sUserFuncPlugin = 'tx_hivecptcntgooglemap';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'group',
    'internal_type' => 'db',
    'allowed' => $sTable,
    'foreign_table' => $sTable,
    'foreign_table_where' => 'AND sys_language_uid IN (-1,0) AND deleted = 0 AND hidden = 0',
    'size' => 1,
    'minitems' => 1,
    'maxitems' => 1,
    'multiple' => 0,
    'suggestOptions' => [
        'default' => [
            'searchWholePhrase' => 1,
        ],
        $sTable => [
            'searchCondition' => 'sys_language_uid IN (-1,0)'
        ],
    ],
];
$sPidList = (string) HIVE\HiveUserfuncs\UserFunc\StorageUserFunc::getStoragePidListForModelInPlugin($sUserFuncModel, $sUserFuncPlugin);
if ($sPidList != '') {
    $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['suggestOptions']['default']['pidList'] = $sPidList;
}

/*
 * Render
 */
$sColumn = 'render';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'select',
    'renderType' => 'selectSingle',
    'items' => [
        ['---',''],
        ['HIVE\\HiveCptCntGoogleMap\\Domain\\Model\\InfoWindow','HIVE\\HiveCptCntGoogleMap\\Domain\\Model\\InfoWindow'],
        ['HIVE\\HiveExtAddress\\Domain\\Model\\Address (Not yet implemented)','HIVE\\HiveExtAddress\\Domain\\Model\\Address'],
        ['HIVE\\HiveExtAddress\\Domain\\Model\\Location (Not yet implemented)','HIVE\\HiveExtAddress\\Domain\\Model\\Location'],
    ],
    'default' => '',
];