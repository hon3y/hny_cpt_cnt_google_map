
plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprender {
    view {
        # cat=plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprender/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_google_map/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprender/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_google_map/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprender/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_google_map/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprender//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprequest {
    view {
        # cat=plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprequest/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_google_map/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprequest/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_google_map/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprequest/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_google_map/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntgooglemap_hivecptcntgooglemaprequest//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hivecptcntgooglemap {
        model {
            HIVE\HiveCptCntGoogleMap\Domain\Model\Map {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveCptCntGoogleMap\Domain\Model\Markericon {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveCptCntGoogleMap\Domain\Model\InfoWindow {
                persistence {
                    storagePid =
                }
            }
        }
    }
}

plugin {
    tx_hivecptcntgooglemap_hivecptcntgooglemaprender {
        persistence {
            storagePid =
        }
        settings {
            map {
                js {
                    options = /typo3conf/ext/hive_cpt_cnt_google_map/Resources/Public/Map/Json/options.json
                    styles = /typo3conf/ext/hive_cpt_cnt_google_map/Resources/Public/Map/Json/styles.json
                }
            }
            fusionTablesLayer {
                tableDocId =
                tabelSelectField =
                js {
                    styles =
                }
            }
        }
    }
}